package id.everspin.backend_sample.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.everspin.backend_sample.entity.*;
import id.everspin.backend_sample.entity.Response.EncryptResponse;
import id.everspin.backend_sample.repository.UserRepository;
import id.everspin.backend_sample.service.EversafeService;
import id.everspin.backend_sample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Random;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    EversafeService eversafeService;

    @Autowired
    private ObjectMapper objectMapper;


    @RequestMapping(value = "/everspin/user_login", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> getUserByLogin(@RequestBody LoginRequest loginRequest) {
        /*String userAccess = userRepository.findByUserLogin(userName, userPassword).getAccess();
        List<String> accessItems = new ArrayList<String>(Arrays.asList(userAccess.split("\\s*,\\s*")));*/
        int isActive = userRepository.findByUserLogin(loginRequest.getUser_name(), loginRequest.getUser_password()).getIsActive();

        if (isActive == 0) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("result", "failed");
            hashMap.put("isactive", 0);
            hashMap.put("user_detail", userRepository.findByUserLogin(loginRequest.getUser_name(), loginRequest.getUser_password()));

            return ResponseEntity.ok(hashMap);
        }
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("result", "success");
            hashMap.put("isactive", 1);
            hashMap.put("user_detail", userRepository.findByUserLogin(loginRequest.getUser_name(), loginRequest.getUser_password()));

            return ResponseEntity.ok(hashMap);
    }

    //with eversafe
    @RequestMapping(value = "/everspin/user_login_eversafe", method = RequestMethod.POST)
    public ResponseEntity<EncryptResponse> getUserByLoginWithEversafe(@RequestBody EversafeSecureStorageRequestDto requestDto) {
        return eversafeService.getLoginServiceWithEversafe(requestDto);
    }

    @RequestMapping(value = "/everspin/request_otp", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> requestOtp(@RequestParam("user_id") long userId) {

        int newOtp = Integer.parseInt(String.valueOf(generateOTP(6)));

        userService.updateUserOtp(newOtp, userId);
        userRepository.findByUserId(userId).setOtpCode(newOtp);
        return userService.findByUserId(userId);

    }

    @RequestMapping(value = "/everspin/user_otp_check", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> getUserOtp(@RequestParam("user_id") long userId, @RequestParam("user_otp") String userOtp) {
        int getUserOtp = userRepository.findByUserId(userId).getOtpCode();

        System.out.print(getUserOtp);

        if (userOtp.equalsIgnoreCase(String.valueOf(getUserOtp))) {
            userService.updateUserActivation(userId);
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("result", "success");
            return ResponseEntity.ok(hashMap);
        }
        HashMap<String, Object> hashMap2 = new HashMap<>();
        hashMap2.put("result", "failed");
        return ResponseEntity.ok(hashMap2);

    }

    @RequestMapping(value = "/everspin/reset_password", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> updateUserPassword(@RequestParam("user_id") long userId, @RequestParam("new_password") String newPassword) {

        userService.updateUserPassoword(userId, newPassword);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("result", "success");

        return ResponseEntity.ok(hashMap);

    }

    @RequestMapping(value = "/everspin/detail_user", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> getUserDetail(@RequestBody UserDetailRequest userDetailRequest) {

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("result", "success");
        hashMap.put("user_detail", userRepository.findByUserId(userDetailRequest.getUser_id()));
        return ResponseEntity.ok(hashMap);

    }

    //user detail with eversafe
    @RequestMapping(value = "/everspin/detail_user_with_eversafe", method = RequestMethod.POST)
    public ResponseEntity<EncryptResponse> getUserDetailWithEversafe(@RequestBody EversafeSecureStorageRequestDto requestDto) {
        return eversafeService.getUserDetailServiceWithEversafe(requestDto);
    }

    private static char[] generateOTP(int length) {
        String numbers = "123456789";
        Random random = new Random();
        char[] otp = new char[length];

        for(int i = 0; i< length ; i++) {
            otp[i] = numbers.charAt(random.nextInt(numbers.length()));
        }
        return otp;
    }
}
