package id.everspin.backend_sample.controller;

import id.everspin.backend_sample.entity.EversafeSecureStorageRequestDto;
import id.everspin.backend_sample.entity.EversafeSecureStorageResponseDto;
import id.everspin.backend_sample.service.EversafeService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/eversafe")
@RestController
public class EversafeController {

    private EversafeService eversafeService;

    public EversafeController(EversafeService eversafeService) {
        System.out.println("EversafeController");
        this.eversafeService = eversafeService;
    }

    @PostMapping(value = "/secure-storage")
    public ResponseEntity<EversafeSecureStorageResponseDto> getSecureStorage(@RequestBody EversafeSecureStorageRequestDto eversafeSecureStorageRequestDto) throws Exception {
        System.out.println("EversafeController: EversafeSecureStorageResponseDto");
        return eversafeService.getSecureStorage(eversafeSecureStorageRequestDto);
    }

    @GetMapping(value = "/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        System.out.println("hello: hello");
        return String.format("Hello %s!", name);
    }
}
