package id.everspin.backend_sample.controller;

import id.everspin.backend_sample.entity.EversafeSecureStorageRequestDto;
import id.everspin.backend_sample.entity.Response.EncryptResponse;
import id.everspin.backend_sample.entity.TransactionRequest;
import id.everspin.backend_sample.entity.UserDetailRequest;
import id.everspin.backend_sample.repository.TransaksiRepository;
import id.everspin.backend_sample.repository.UserRepository;
import id.everspin.backend_sample.service.EversafeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.HashMap;

@RestController
public class TransaksiController {

    @Autowired
    private TransaksiRepository transaksiRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    EversafeService eversafeService;

    @RequestMapping(value = "/everspin/all_transaksi", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> getAllTransaksi(){
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("result", "success");
        hashMap.put("dataTransaksi", transaksiRepository.findAll());
        return ResponseEntity.ok(hashMap);
    }

    @RequestMapping(value = "/everspin/history_transaction", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> getHistoryTransaksiByUserCode(@RequestBody UserDetailRequest userDetailRequest) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("result", "success");
        hashMap.put("historyDetailTransaksi", transaksiRepository.findHistoryTransaksiByUserCode(userDetailRequest.getUser_code()));
        return ResponseEntity.ok(hashMap);

    }

    @RequestMapping(value = "/everspin/history_transaction_with_eversafe", method = RequestMethod.POST)
    public ResponseEntity<EncryptResponse> getHistoryTransaksiByUserCodeWithEversafe(@RequestBody EversafeSecureStorageRequestDto requestDto) {
        return eversafeService.getHistoryTransaction(requestDto);
    }

    @Transactional
    @RequestMapping(value = "/everspin/send_transaksi", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> sendTransaksi(@RequestBody TransactionRequest transactionRequest) {
        Date currentDate = new Date(System.currentTimeMillis());

        int saldoSender = userRepository.findUserByCode(transactionRequest.getSender_code()).getSaldo();
        int saldoReceiver = userRepository.findUserByCode(transactionRequest.getReceiver_code()).getSaldo();

        int saldoSenderFinal = saldoSender - transactionRequest.getNominal();
        int saldoReciverFinal = saldoReceiver + transactionRequest.getNominal();

        userRepository.updateSaldoUser(saldoSenderFinal, transactionRequest.getSender_code());
        userRepository.updateSaldoUser(saldoReciverFinal, transactionRequest.getReceiver_code());


        transaksiRepository.sendTransaksi(transactionRequest.getSender_code(), transactionRequest.getReceiver_code(),
                currentDate, transactionRequest.getNominal());


        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("result", "success");
//        hashMap.put("userDetail", userSender);
        return ResponseEntity.ok(hashMap);
    }

    @Transactional
    @RequestMapping(value = "/everspin/send_transaksi_with_eversafe", method = RequestMethod.POST)
    public ResponseEntity<EncryptResponse> sendTransaksiWithEversafe(@RequestBody EversafeSecureStorageRequestDto requestDto) {
        return eversafeService.doTransactionServiceWithEversafe(requestDto);
    }
}
