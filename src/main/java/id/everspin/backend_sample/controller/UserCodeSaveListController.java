package id.everspin.backend_sample.controller;

import id.everspin.backend_sample.entity.EversafeSecureStorageRequestDto;
import id.everspin.backend_sample.entity.Response.EncryptResponse;
import id.everspin.backend_sample.entity.db.User;
import id.everspin.backend_sample.repository.UserCodeSaveListRepository;
import id.everspin.backend_sample.repository.UserRepository;
import id.everspin.backend_sample.service.EversafeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.HashMap;

@RestController
public class UserCodeSaveListController {

    @Autowired
    private UserCodeSaveListController userCodeSaveListController;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    EversafeService eversafeService;

    @Autowired
    private UserCodeSaveListRepository userCodeSaveListRepository;

    @RequestMapping(value = "/everspin/code_saved_list", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> getSavedListByUserCode(@RequestParam("userCode") String userCode) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("result", "success");
        hashMap.put("savedList", userCodeSaveListRepository.findUserByUserCodeSaver(userCode));
        return ResponseEntity.ok(hashMap);
    }

    @RequestMapping(value = "/everspin/code_saved_list_with_eversafe", method = RequestMethod.POST)
    public ResponseEntity<EncryptResponse> getSavedListByUserCodeWithEversafe(@RequestBody EversafeSecureStorageRequestDto requestDto) {
        return eversafeService.getUserCodeListServiceWithEversafe(requestDto);
    }



    @Transactional
    @RequestMapping(value = "/everspin/insert_code", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> insertCode(@RequestParam("userCodeSaver") String userCodeSaver, @RequestParam("userCodeSaved") String userCodeSaved) {

        User user = userRepository.findUserByCode(userCodeSaved);
        String name = user.getName();

        userCodeSaveListRepository.userCodeSave(userCodeSaver, userCodeSaved, name);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("result", "success");
        return ResponseEntity.ok(hashMap);
    }
}
