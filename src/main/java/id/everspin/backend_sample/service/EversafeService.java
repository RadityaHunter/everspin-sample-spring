package id.everspin.backend_sample.service;


import com.google.gson.Gson;
import everspin.external.org.apache.commons.codec.binary.Base64;
import everspin.external.org.apache.http.HttpStatus;
import id.everspin.backend_sample.entity.*;
import id.everspin.backend_sample.entity.Response.EncryptResponse;
import id.everspin.backend_sample.entity.db.Transaction;
import id.everspin.backend_sample.entity.db.User;
import id.everspin.backend_sample.entity.db.UserCodeSaveList;
import id.everspin.backend_sample.repository.TransaksiRepository;
import id.everspin.backend_sample.repository.UserCodeSaveListRepository;
import id.everspin.backend_sample.repository.UserRepository;
import kr.co.everspin.eversafe.EversafeClient;
import kr.co.everspin.eversafe.EversafeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

@Service
public class EversafeService {

    private EversafeManager eversafeManager;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserCodeSaveListRepository userCodeSaveListRepository;

    @Autowired
    private TransaksiRepository transaksiRepository;

    public EversafeService(EversafeManager eversafeManager) {
        this.eversafeManager = eversafeManager;
        System.out.print("eversafeManager");
    }

    public ResponseEntity<EversafeSecureStorageResponseDto> getSecureStorage(
            EversafeSecureStorageRequestDto eversafeSecureStorageRequestDto) {

        EversafeSecureStorageResponseDto eversafeSecureStorageResponseDto = new EversafeSecureStorageResponseDto();

        System.out.println("eversafeManager: evToken : " + eversafeSecureStorageRequestDto.getEvToken());
        System.out.println("eversafeManager: EvEncDesc : " + eversafeSecureStorageRequestDto.getEvEncDesc());

        try {

            EversafeClient eversafeClient = eversafeManager.getEversafeClient();

            EversafeResponse encryptionKeyResponse = eversafeClient
                    .getEncryptionKey(eversafeSecureStorageRequestDto.getEvEncDesc());
            if (!encryptionKeyResponse.getResult() && encryptionKeyResponse.getKey() == null) {
                //If Failed, error msg "Failed to Get Key"
                eversafeSecureStorageResponseDto.setResult(encryptionKeyResponse.getResult());
                eversafeSecureStorageResponseDto.setErrorMsg(encryptionKeyResponse.getContent());
                return ResponseEntity.ok(eversafeSecureStorageResponseDto);
            }

            String encryptionKey = encryptionKeyResponse.getKey();

            byte[] decryptToken = Base64.decodeBase64(eversafeSecureStorageRequestDto.getEvToken());
            byte[] eversafeToken = eversafeClient.decrypt(decryptToken, encryptionKey);
            System.out.println("eversafeManager: encryptionKey : " + encryptionKey);

            EversafeResponse tokenResponse = eversafeClient.verify(eversafeToken);
            if (!tokenResponse.getResult()) {
                // Error Msg "Failed to Verify Token"
                eversafeSecureStorageResponseDto.setResult(tokenResponse.getResult());
                eversafeSecureStorageResponseDto.setErrorMsg(tokenResponse.getContent());
                return ResponseEntity.ok(eversafeSecureStorageResponseDto);
            }

            eversafeSecureStorageResponseDto.setResult(tokenResponse.getResult());

            // jerry
            // String secureStorage = ((Object) tokenResponse).getSecureStorageInfo();
            String secureStorage = tokenResponse.getSecureStorageInfo();
            byte[] encryptSecureStorage = eversafeClient.encrypt(secureStorage.getBytes(), encryptionKey);
            String base64SecureStorage = Base64.encodeBase64String(encryptSecureStorage);
            eversafeSecureStorageResponseDto.setSecureStorage(base64SecureStorage);
            return ResponseEntity.ok(eversafeSecureStorageResponseDto);

        } catch (Exception ex) {
            eversafeSecureStorageResponseDto.setResult(false);
            eversafeSecureStorageResponseDto.setErrorMsg(ex.getMessage());
            return ResponseEntity.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).body(eversafeSecureStorageResponseDto);
        }

    }

    //eversafe service
    public EversafeSecureStorageResponseDto getServiceWithEversafe(EversafeSecureStorageRequestDto eversafeSecureStorageRequestDto,
                                                                   EversafeClient eversafeClient) {
        EversafeSecureStorageResponseDto responseDto = new EversafeSecureStorageResponseDto();

        try {

            EversafeResponse eversafeKeyResponse = eversafeClient.getEncryptionKey(eversafeSecureStorageRequestDto.getEvEncDesc());

            if (!eversafeKeyResponse.getResult() && eversafeKeyResponse.getKey() == null) {
                //If Failed, error msg "Failed to Get Key"
                responseDto.setResult(eversafeKeyResponse.getResult());
                responseDto.setErrorMsg(eversafeKeyResponse.getContent());
                return responseDto;
            }

            String encryptionKey = eversafeKeyResponse.getKey();

            byte[] decryptToken = Base64.decodeBase64(eversafeSecureStorageRequestDto.getEvToken());
            byte[] eversafeToken = eversafeClient.decrypt(decryptToken, encryptionKey);

            byte[] payloadToken = Base64.decodeBase64(eversafeSecureStorageRequestDto.getPayload());
            byte[] payloadDecrypt = eversafeClient.decrypt(payloadToken, encryptionKey);
            String eversafePayload = new String(payloadDecrypt, StandardCharsets.UTF_8);

            EversafeResponse tokenResponse = eversafeClient.verify(eversafeToken);
            if (!tokenResponse.getResult()) {
                // Error Msg "Failed to Verify Token"
                responseDto.setResult(tokenResponse.getResult());
                responseDto.setErrorMsg(tokenResponse.getContent());
                return responseDto;
            }

            responseDto.setResult(tokenResponse.getResult());
            responseDto.setPayload(eversafePayload);
            responseDto.setKey(encryptionKey);

            return responseDto;
        } catch (Exception e) {
            responseDto.setResult(false);
            responseDto.setErrorMsg(e.getMessage());
            return responseDto;
        }
    }

    //login with eversafe service
    public ResponseEntity<EncryptResponse> getLoginServiceWithEversafe(EversafeSecureStorageRequestDto requestDto) {

        EncryptResponse encryptResponse = new EncryptResponse();

        try {
            EversafeClient eversafeClient = eversafeManager.getEversafeClient();
            EversafeSecureStorageResponseDto storageResponseDto = getServiceWithEversafe(requestDto, eversafeClient);

            if (storageResponseDto.getResult()) {
                Gson gson = new Gson();

                LoginRequest loginRequest = gson.fromJson(storageResponseDto.getPayload(), LoginRequest.class);
                User user = userRepository.findByUserLogin(loginRequest.getUser_name(), loginRequest.getUser_password());

                if (user.getIsActive() != 0) {
                    byte[] bytes = user.toString().getBytes();
                    String encryptPayload = Base64.encodeBase64String(eversafeClient.encrypt(bytes, storageResponseDto.getKey()));

                    encryptResponse.setResult(true);
                    encryptResponse.setPayload(encryptPayload);

                    return ResponseEntity.ok(encryptResponse);
                } else {
                    return ResponseEntity.ok(encryptResponse);
                }
            } else {
                return ResponseEntity.ok(encryptResponse);
            }
        } catch (Exception e) {
            encryptResponse.setResult(false);
            encryptResponse.setErorMsg(e.getMessage());
            return ResponseEntity.status(org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR).body(encryptResponse);
        }
    }


    public ResponseEntity<EncryptResponse> getUserDetailServiceWithEversafe(EversafeSecureStorageRequestDto requestDto) {

        EncryptResponse encryptResponse = new EncryptResponse();

        try {
            EversafeClient eversafeClient = eversafeManager.getEversafeClient();
            EversafeSecureStorageResponseDto storageResponseDto = getServiceWithEversafe(requestDto, eversafeClient);

            if (storageResponseDto.getResult()) {
                Gson gson = new Gson();

                UserDetailRequest userDetailRequest = gson.fromJson(storageResponseDto.getPayload(), UserDetailRequest.class);
                User user = userRepository.findByUserId(userDetailRequest.getUser_id());

                if (user.getIsActive() != 0) {
                    byte[] bytes = user.toString().getBytes();
                    String encryptPayload = Base64.encodeBase64String(eversafeClient.encrypt(bytes, storageResponseDto.getKey()));

                    encryptResponse.setResult(true);
                    encryptResponse.setPayload(encryptPayload);

                    return ResponseEntity.ok(encryptResponse);
                } else {
                    return ResponseEntity.ok(encryptResponse);
                }
            } else {
                return ResponseEntity.ok(encryptResponse);
            }
        } catch (Exception e) {
            encryptResponse.setResult(false);
            encryptResponse.setErorMsg(e.getMessage());
            return ResponseEntity.status(org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR).body(encryptResponse);
        }
    }

    public ResponseEntity<EncryptResponse> getUserCodeListServiceWithEversafe(EversafeSecureStorageRequestDto requestDto) {

        EncryptResponse encryptResponse = new EncryptResponse();

        try {
            EversafeClient eversafeClient = eversafeManager.getEversafeClient();
            EversafeSecureStorageResponseDto storageResponseDto = getServiceWithEversafe(requestDto, eversafeClient);

            if (storageResponseDto.getResult()) {
                Gson gson = new Gson();

                UserDetailRequest userDetailRequest = gson.fromJson(storageResponseDto.getPayload(), UserDetailRequest.class);
                List<UserCodeSaveList> userCodeSaveLists = userCodeSaveListRepository.findUserByUserCodeSaver(userDetailRequest.getUser_code());

                if (userCodeSaveLists.size() > 0) {
                    byte[] bytes = userCodeSaveLists.toString().getBytes();
                    String encryptPayload = Base64.encodeBase64String(eversafeClient.encrypt(bytes, storageResponseDto.getKey()));

                    encryptResponse.setResult(true);
                    encryptResponse.setPayload(encryptPayload);

                    return ResponseEntity.ok(encryptResponse);
                } else {
                    return ResponseEntity.ok(encryptResponse);
                }
            } else {
                return ResponseEntity.ok(encryptResponse);
            }
        } catch (Exception e) {
            encryptResponse.setResult(false);
            encryptResponse.setErorMsg(e.getMessage());
            return ResponseEntity.status(org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR).body(encryptResponse);
        }
    }

    @Transactional
    public ResponseEntity<EncryptResponse> doTransactionServiceWithEversafe(EversafeSecureStorageRequestDto requestDto) {

        Date currentDate = new Date(System.currentTimeMillis());

        EncryptResponse encryptResponse = new EncryptResponse();

        try {
            EversafeClient eversafeClient = eversafeManager.getEversafeClient();
            EversafeSecureStorageResponseDto storageResponseDto = getServiceWithEversafe(requestDto, eversafeClient);

            if (storageResponseDto.getResult()) {
                Gson gson = new Gson();

                TransactionRequest transactionRequest = gson.fromJson(storageResponseDto.getPayload(), TransactionRequest.class);
                transaksiRepository.sendTransaksi(transactionRequest.getSender_code(),
                        transactionRequest.getReceiver_code(), currentDate, transactionRequest.getNominal());

                encryptResponse.setResult(true);

                return ResponseEntity.ok(encryptResponse);

            } else {
                encryptResponse.setResult(false);
                return ResponseEntity.ok(encryptResponse);
            }
        } catch (Exception e) {
            encryptResponse.setResult(false);
            encryptResponse.setErorMsg(e.getMessage());
            return ResponseEntity.status(org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR).body(encryptResponse);
        }
    }

    public ResponseEntity<EncryptResponse> getHistoryTransaction(EversafeSecureStorageRequestDto requestDto) {

        EncryptResponse encryptResponse = new EncryptResponse();

        try {
            EversafeClient eversafeClient = eversafeManager.getEversafeClient();
            EversafeSecureStorageResponseDto storageResponseDto = getServiceWithEversafe(requestDto, eversafeClient);

            if (storageResponseDto.getResult()) {
                Gson gson = new Gson();

                UserDetailRequest userDetailRequest = gson.fromJson(storageResponseDto.getPayload(), UserDetailRequest.class);
                List<Transaction> transactionList = transaksiRepository.findHistoryTransaksiByUserCode(userDetailRequest.getUser_code());
                if (transactionList.size() > 0) {
                    byte[] bytes = transactionList.toString().getBytes();
                    String encryptPayload = Base64.encodeBase64String(eversafeClient.encrypt(bytes, storageResponseDto.getKey()));

                    encryptResponse.setResult(true);
                    encryptResponse.setPayload(encryptPayload);
                    return ResponseEntity.ok(encryptResponse);

                } else {
                    encryptResponse.setResult(false);
                    encryptResponse.setErorMsg("There is no transaction record!");
                    return ResponseEntity.ok(encryptResponse);
                }
            } else {
                encryptResponse.setResult(false);
                encryptResponse.setErorMsg(storageResponseDto.getErrorMsg());
                return ResponseEntity.ok(encryptResponse);
            }
        } catch (Exception e) {
            encryptResponse.setResult(false);
            encryptResponse.setErorMsg(e.getMessage());
            return ResponseEntity.status(org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR).body(encryptResponse);
        }
    }



}
