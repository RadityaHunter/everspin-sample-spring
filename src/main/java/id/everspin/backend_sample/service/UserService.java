package id.everspin.backend_sample.service;

import id.everspin.backend_sample.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public ResponseEntity<HashMap<String, Object>> findByUserId(long id) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("result", "success");
        hashMap.put("timer", 420);
        hashMap.put("userDetail", userRepository.findById(id));

        return ResponseEntity.ok(hashMap);
    }

    @Transactional
    public void updateUserOtp(int newOtp, long userId) {
        userRepository.generateUserOtp(newOtp, userId);
//        entityManager.clear();
    }

    @Transactional
    public void updateUserPassoword(long userId, String passoword) {
        userRepository.updateNewPassowrd(passoword, userId);
//        entityManager.clear();
    }

    @Transactional
    public void updateUserActivation(long userId) {
        userRepository.setUserActiveById(userId);
//        entityManager.clear();
    }
}
