package id.everspin.backend_sample.repository;

import id.everspin.backend_sample.entity.db.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select a from User a where a.username= :userName and a.password= :userPassword")
    User findByUserLogin(@Param("userName") String userName, @Param("userPassword") String userPassword);

    @Modifying
    @Query(nativeQuery = true,
            value = "update users set otp_code = ?1 where  id= ?2")
    void generateUserOtp(int newOtp, long userId);

    @Modifying
    @Query(nativeQuery = true,
            value = "update users set password = ?1 where  id= ?2")
    void updateNewPassowrd(String password, long userId);

    @Modifying
    @Query(nativeQuery = true,
            value = "update users set isactive = 1 where  id= ?1")
    void setUserActiveById(long userId);

    @Query(value = "select a from User a where  a.id= :userId")
    User findByUserId(@Param("userId") long userID);


    @Query(value = "select a from User a where a.userCode= :userCode")
    User findUserByCode(String userCode);

    @Modifying
    @Transactional
    @Query(value = "update users set saldo= ?1 where user_code= ?2", nativeQuery = true)
    void updateSaldoUser(int saldo, String userCode);

/*
    @Query(nativeQuery = true,
            value = "update users set otp_code = ?1 where id= ?2")
    User generateUserOtp(int newOtp, long userId);*/
}
