package id.everspin.backend_sample.repository;

import id.everspin.backend_sample.entity.db.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
public interface TransaksiRepository extends JpaRepository<Transaction, Long> {

    @Query(value = "select a from Transaction a where a.userCodePengirim= :userCode or a.userCodePenerima= :userCode")
    List<Transaction> findHistoryTransaksiByUserCode(@Param("userCode") String userCode);


    @Modifying
    @Transactional
    @Query(value = "insert into transaksi (user_code_penerima , user_code_pengirim , tgl_trans , nominal) values(:userCodeSender, :userCodeReceiver, :date, :nominal)", nativeQuery = true)
    void sendTransaksi(@Param("userCodeSender") String userCodeSender, @Param("userCodeReceiver") String userCodeReceiver, @Param("date") Date date, @Param("nominal") int nominal);

}
