package id.everspin.backend_sample.repository;

import id.everspin.backend_sample.entity.db.UserCodeSaveList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface UserCodeSaveListRepository extends JpaRepository<UserCodeSaveList, Long> {

    @Query(value = "select a from UserCodeSaveList a where a.userCodeSaver= :userCode")
    List<UserCodeSaveList> findUserByUserCodeSaver(@Param("userCode") String userCode);

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "insert into user_code_save_list (user_code_saver, user_code_saved, name_saved) values (:userCodeSaver, :userCodeSaved, :nameSaved) ")
    void userCodeSave(@Param("userCodeSaver") String userCodeSaver, @Param("userCodeSaved") String userCodeSaved, @Param("nameSaved") String nameSaved);


}
