package id.everspin.backend_sample.entity.db;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_code_save_list")
public class UserCodeSaveList {
    @Id
    @Column(name = "id_user_code_list")
    private Long idUserCodeList;

    @Column(name = "user_code_saver")
    private String userCodeSaver;

    @Column(name = "user_code_saved")
    private String userCodeSaved;

    @Column(name = "name_saved")
    private String nameSaved;

    public Long getIdUserCodeList() {
        return idUserCodeList;
    }

    public void setIdUserCodeList(Long idUserCodeList) {
        this.idUserCodeList = idUserCodeList;
    }

    public String getUserCodeSaver() {
        return userCodeSaver;
    }

    public void setUserCodeSaver(String userCodeSaver) {
        this.userCodeSaver = userCodeSaver;
    }

    public String getUserCodeSaved() {
        return userCodeSaved;
    }

    public void setUserCodeSaved(String userCodeSaved) {
        this.userCodeSaved = userCodeSaved;
    }

    public String getNameSaved() {
        return nameSaved;
    }

    public void setNameSaved(String nameSaved) {
        this.nameSaved = nameSaved;
    }
}
