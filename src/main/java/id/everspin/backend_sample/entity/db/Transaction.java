package id.everspin.backend_sample.entity.db;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

@Entity
@Table(name ="transaksi")
public class Transaction {
    @Id
    @Column(name = "id_transaksi")
    private Long idTrans;

    @Column(name = "user_code_pengirim")
    private String userCodePengirim;

    @Column(name = "user_code_penerima")
    private String userCodePenerima;

    @Column(name = "tgl_trans")
    private String tglTrans;

    @Column(name = "nominal")
    private BigInteger nominal;

    public Long getIdTrans() {
        return idTrans;
    }

    public void setIdTrans(Long idTrans) {
        this.idTrans = idTrans;
    }

    public String getUserCodePengirim() {
        return userCodePengirim;
    }

    public void setUserCodePengirim(String userCodePengirim) {
        this.userCodePengirim = userCodePengirim;
    }

    public String getUserCodePenerima() {
        return userCodePenerima;
    }

    public void setUserCodePenerima(String userCodePenerima) {
        this.userCodePenerima = userCodePenerima;
    }

    public String getTglTrans() {
        return tglTrans;
    }

    public void setTglTrans(String tglTrans) {
        this.tglTrans = tglTrans;
    }

    public BigInteger getNominal() {
        return nominal;
    }

    public void setNominal(BigInteger nominal) {
        this.nominal = nominal;
    }
}
