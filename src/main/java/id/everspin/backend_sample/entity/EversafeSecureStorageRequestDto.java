package id.everspin.backend_sample.entity;

public class EversafeSecureStorageRequestDto {
    private String evToken;
    private String evEncDesc;
    private String payload;

    public void setEvToken(String evToken) {
        this.evToken = evToken;
    }

    public String getEvToken() {
        return evToken;
    }

    public void setEvEncDesc(String evEncDesc) {
        this.evEncDesc = evEncDesc;
    }

    public String getEvEncDesc() {
        return evEncDesc;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
