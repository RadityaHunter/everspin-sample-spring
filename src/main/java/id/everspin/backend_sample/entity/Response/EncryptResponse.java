package id.everspin.backend_sample.entity.Response;

public class EncryptResponse {

    boolean result;
    String erorMsg;
    String payload;

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getErorMsg() {
        return erorMsg;
    }

    public void setErorMsg(String erorMsg) {
        this.erorMsg = erorMsg;
    }
}
