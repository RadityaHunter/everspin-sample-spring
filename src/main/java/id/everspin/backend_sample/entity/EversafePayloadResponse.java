package id.everspin.backend_sample.entity;

public class EversafePayloadResponse {

    private String key;
    private String payload;

    public EversafePayloadResponse(String key, String payload) {
        this.key = key;
        this.payload = payload;
    }

    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }
    public String getPayload() {
        return payload;
    }
    public void setPayload(String payload) {
        this.payload = payload;
    }
}
