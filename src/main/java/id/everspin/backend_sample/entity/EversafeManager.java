package id.everspin.backend_sample.entity;


import kr.co.everspin.eversafe.EversafeClient;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class EversafeManager {

    EversafeClient eversafeClient = null;

    protected String eversafeClientBase = "/Users/dnr/Documents/Everspin/Backend Properties/JavaLibraryEversafe";

    @PostConstruct
    public void init() throws Exception {
        System.out.println("EversafeManager");
        initialize();
    }

    public void initialize() throws Exception {
        if (eversafeClient != null && eversafeClient.getInitialized()) {
            return;
        }
        System.out.println("initialize");
        initializeEversafeFromPath();

    }

    public void initializeEversafeFromPath() throws Exception {
        eversafeClient = new EversafeClient();
        eversafeClient.initialize(eversafeClientBase);
        System.out.println("initializeEversafeFromPath");
    }

    public EversafeClient getEversafeClient() throws Exception {

        if (eversafeClient == null || !eversafeClient.getInitialized()) {
            System.out.println("getEversafeClient");
            initialize();
        }
        System.out.println("not run getEversafeClient");
//        System.out.println("not run getEversafeClient");

        return eversafeClient;
    }
}
