package id.everspin.backend_sample.entity;

public class EversafeSecureStorageResponseDto {
    private boolean result;
    private String errorMsg;
    private String secureStorage;
    private String payload;
    private String key; //for easier service setting

    public void setResult(boolean result) {
        this.result = result;
    }

    public boolean getResult() {
        return result;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setSecureStorage(String secureStorage) {
        this.secureStorage = secureStorage;
    }

    public String getSecureStorage() {
        return secureStorage;
    }

    public boolean isResult() {
        return result;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
